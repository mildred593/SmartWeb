Access Control Lists
====================

An ACL is composed of:

- an order, a number
- a bubble position like DOM Events, up or down
- an optional matching method
- an optional matching user
- an action

Actions can be:

- continue bubbling (skip other ACLs for the current level)
- stop bubbling and accept
- stop bubbling and deny

ACL refers to the resource described by the graph in which they are.

Fetch ACLs
----------


	SELECT ?acl ?page coalesce(?user, nil) coalesce(?order, 0) coalesce(?bubble, sw:BubbleUp) coalesce(?method, nil) coalesce(?action, sw:ContinueBubbling)
	GRAPH ?page {
		?acl a sw:ACL .
		OPTIONAL { ?acl sw:user ?user }
		OPTIONAL { ?acl sw:order ?order }
		OPTIONAL { ?acl sw:bubble ?bubble }
		OPTIONAL { ?acl sw:method ?method }
		OPTIONAL { ?acl sw:action ?action }
	}

	PREFIX sw: <tag:mildred.fr,2015-05:SmartWeb#>

	SELECT ?page ?acl ?order ?bubble ?method ?user ?action
	FROM </>
	FROM </dir/>
	FROM </dir/page>
	?page ?acl ?user ?auth ?act
	%1q
	WHERE {
		VALUES ?page { %2q }
		?acl
			a        sw:ACL ;
			sw:about ?page ;
			sw:user+ ?user ;
			?auth    ?act .
		VALUES ?user { %4u sw:Anonymous }
		VALUES ?auth { sw:allow sw:deny }
		VALUES ?act { %3s sw:Default }
	}
    
